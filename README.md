# Projet Balzor
Ce projet a été réalisé durant le cours de Blazor de 2ème année de BUT à l'IUT Clermont Auvergne. Minecraft Web est une application qui permet de simuler un inventaire Minecraft en web avec la liste de tous les objets du jeu.

## Ma configuration
* Visual Studio 2022

## Utilisation 
Une fois le dépôt cloné, il vous faudra configurer la solution du projet.

    1. Pour cela click droit sur la solution pour accéder aux propriétés de la solution. 
![Image clique droit](/docs/Menu.png)

    2. Une fois, cela fait sélectionner "multiple startup projects:" et définissez les actions des projets en "Start". 
![Image clique droit](/docs/Config.png)

    3. Vous pouvez enfin appliquer et lancer le projet.
![Image clique droit](/docs/Start.png)

## Développeurs 
* Louis Dufour : Louis.Dufour@etu.uca.fr

# Plus d'information
Si vous voulez plus d'information, vous pouvez consulter le dossier "docs".