﻿namespace ValblazeProject.Models
{
    public class Inventory
    {
        public string itemName { get; set; }
        public int numberItem { get; set; }
        public int position { get; set; }
    }
}
