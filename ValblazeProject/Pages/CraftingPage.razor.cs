﻿using Microsoft.AspNetCore.Components;
using ValblazeProject.Components;
using Microsoft.Extensions.Localization;
using ValblazeProject.Models;
using ValblazeProject.Services;

namespace ValblazeProject.Pages
{
    public partial class CraftingPage
    {

        [Inject]
        public IDataService DataService { get; set; }
        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();

            StateHasChanged();
        }
    }
}
