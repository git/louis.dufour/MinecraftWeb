﻿using ValblazeProject.Models;
using ValblazeProject.Components;

namespace ValblazeProject.Services
{
    public interface IDataService
    {
        // Crafting
        Task Add(ItemModel model);
        Task<int> Count();
        Task<List<Item>> List(int currentPage, int pageSize);
        Task<List<Item>> List();
        Task<Item> GetById(int id);
        Task Update(int id, ItemModel model);
        Task Delete(int id);
        Task<List<CraftingRecipe>> GetRecipes();

        // Inventory
        Task<List<Inventory>> GetInventory();
        Task SupprInventory(Inventory item);
        Task PutInventory(Inventory item);
        Task PostInventory(Inventory item);

    }
}
