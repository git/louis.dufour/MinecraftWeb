﻿using System.Xml.Linq;
using ValblazeProject.Components;
using ValblazeProject.Factories;
using ValblazeProject.Models;

namespace ValblazeProject.Services
{
    public class DataApiService : IDataService
    {
        private readonly HttpClient _http;

        public DataApiService(
            HttpClient http)
        {
            _http = http;
        }

        /************ Crafting ************/
        public async Task Add(ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            // Save the data
            await _http.PostAsJsonAsync("https://localhost:7234/api/Crafting/", item);
        }

        public async Task<int> Count()
        {
            return await _http.GetFromJsonAsync<int>("https://localhost:7234/api/Crafting/count");
        }

        public async Task<List<Item>> List(int currentPage, int pageSize)
        {
            return await _http.GetFromJsonAsync<List<Item>>($"https://localhost:7234/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
        }

        public async Task<List<Item>> List()
        {
            return await _http.GetFromJsonAsync<List<Item>>($"https://localhost:7234/api/Crafting/all");
        }

        public async Task<Item> GetById(int id)
        {
            return await _http.GetFromJsonAsync<Item>($"https://localhost:7234/api/Crafting/{id}");
        }

        public async Task Update(int id, ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            await _http.PutAsJsonAsync($"https://localhost:7234/api/Crafting/{id}", item);
        }

        public async Task Delete(int id)
        {
            await _http.DeleteAsync($"https://localhost:7234/api/Crafting/{id}");
        }

        public async Task<List<CraftingRecipe>> GetRecipes()
        {
            return await _http.GetFromJsonAsync<List<CraftingRecipe>>("https://localhost:7234/api/Crafting/recipe");
        }

        /************ Inventory ************/
        public async Task SupprInventory(Inventory item)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Content = JsonContent.Create(item),
                Method = HttpMethod.Delete,
                RequestUri = new Uri("https://localhost:7234/api/Inventory")
            };
            await _http.SendAsync(request);
        }

        public async Task<List<Inventory>> GetInventory()
        {
            return await _http.GetFromJsonAsync<List<Inventory>>("https://localhost:7234/api/Inventory");
        }

        public async Task PutInventory(Inventory item)
        {
            await _http.PutAsJsonAsync($"https://localhost:7234/api/Inventory", item);
        }
        public async Task PostInventory(Inventory item)
        {
            // Save the data
            await _http.PostAsJsonAsync("https://localhost:7234/api/Inventory", item);
        }
    }
}
