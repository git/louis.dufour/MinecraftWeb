﻿using Microsoft.AspNetCore.Components;
using System.Text.Json;
using ValblazeProject.Factories;
using ValblazeProject.Models;
using ValblazeProject.Pages;

namespace ValblazeProject.Components
{
    public partial class InventoryItem
    {
        /******************* Initialisation des attributs *******************/
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public Inventaire Parent { get; set; }

        /******************* Méthodes *******************/
        /// <summary>
        /// appel de la méthode lorsqu'un item entre dans un emplacement et envoie une action
        /// </summary>
        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new InventoryAction { Action = "Drag Enter", Item = this.Item, Index = this.Index });
        }

        /// <summary>
        /// appel de la méthode lorsqu'un item quitte un slot et envoie une action
        /// </summary>
        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }
           
            Parent.Actions.Add(new InventoryAction { Action = "Drag Leave", Item = this.Item, Index = this.Index });
        }

        /// <summary>
        /// Cette méthode gère le dépôt d'un item et envoie une action
        /// </summary>
        internal void OnDrop()
        {

            if (NoDrop == true || Parent.CurrentDragItem == null)
            {
                return;
            }
            if (this.Item == null)
            {
                this.Item = ItemFactory.Create(Parent.CurrentDragItem);
            }
            else if (this.Item.Id == Parent.CurrentDragItem.Id)
            {
                if (this.Item.StackSize > this.Item.Num)
                {
                    ItemFactory.Add1(this.Item, this.Item);
                }

            }

            Parent.InventoryItems[this.Index] = this.Item;
            string fileName = "Inventory.json";
            string jsonString = JsonSerializer.Serialize(Parent.InventoryItems);
            File.WriteAllText(fileName, jsonString);


            Parent.Actions.Add(new InventoryAction { Action = "Drop", Item = this.Item, Index = this.Index });
        }

        /// <summary>
        /// appel de la méthode lorsque le darg démarre et envoie une action
        /// </summary>

        private void OnDragStart()
        {

            Parent.CurrentDragItem = this.Item;

            Parent.Actions.Add(new InventoryAction { Action = "Drag Start", Item = this.Item, Index = this.Index });
        }

        /// <summary>
        /// Appel de la méthode quand le drag se termine et envoie une action spécialement en dehors de l'inventaire.
        /// </summary>
        private void OnDragEnd()
        {
            if (Parent.Actions.Last().Action == "Drag Leave")
            {
                this.Item = null;
            }
            Parent.Actions.Add(new InventoryAction { Action = "Delete", Item = this.Item, Index = this.Index });

            Parent.InventoryItems[this.Index] = null;
            string fileName = "Inventory.json";
            string jsonString = JsonSerializer.Serialize(Parent.InventoryItems);
            File.WriteAllText(fileName, jsonString);
        }
    }
}
