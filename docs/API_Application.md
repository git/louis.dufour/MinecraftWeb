# API Minecraft Crafting

![Image clique droit](/API.png)

## C'est quoi une API :

![Image clique droit](/API_Exemple.jpg)

Comme d'après l'exemple ci-dessus une API est un ensemble de `méthodes`*(notre Menu)* qu'on peut utiliser via un service plus couramment `HTTP`.

Celui-ci nous permet donc d'utiliser des données qui ne sont pas stockées chez le client.
Il est donc de notre devoir en tant que développeur de bien les utiliser afin de favoriser l'utilisation des utilisateurs.

Par exemple, si on souhaite, récupère une liste d'éléments, via a une barre de recherche, on va essayer d'importer uniquement les éléments qui nous intéressent et non pas toute la liste pour en suite faire la recherche.

## Son implémentation dans mon projet :

Comme vous l'aurez compris lors de mon projet, j'utilise une API qui a été fournie par mes professeurs.
Elle m'a permis de gérer le fonctionnement d'inventaire Minecraft, mais plus principalement ma `liste d'items`*(éléments)*.

J'ai implémenté les services inventory et le modèle dédier, mais je n'ai pas pu terminer, car j'ai été seul à réaliser ce projet. Par conséquent, j'ai choisi l'alternative de traiter la `grille d'inventaire` côté client. Ce qui n'est peut-être pas forcément une bonne chose, car comme vu précédemment cela risque de défavoriser les petits config et les petites connexions. 

